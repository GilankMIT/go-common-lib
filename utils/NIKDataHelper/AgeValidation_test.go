package NIKDataHelper

import "testing"

func TestGetAgeFromNIK(t *testing.T) {
	//born in 1999, August 04
	age, _ := GetAgeFromNIK("3275020408990021")

	if age < 22 {
		t.Error("age should be more than 22")
	}
}
