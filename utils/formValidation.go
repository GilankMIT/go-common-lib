package utils

import (
	"github.com/albrow/forms"
	"github.com/gin-gonic/gin"
)

type ValidateFormFileRule struct {
	Key             string
	ValidExtensions []string
	IsRequired      *bool
}

func ValidateFormFile(c *gin.Context, rules []ValidateFormFileRule) (*forms.Validator, error) {
	reqBody, err := forms.Parse(c.Request)
	if err != nil {
		return nil, err
	}

	val := reqBody.Validator()

	for _, r := range rules {
		if r.IsRequired == nil || *r.IsRequired {
			val.RequireFile(r.Key)
		}
		val.AcceptFileExts(r.Key, r.ValidExtensions...)
	}

	return val, nil
}
