package main

import (
	"bytes"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/nbdgocean6/go-library/loggers"
	"gitlab.com/nbdgocean6/go-library/utils"

	"github.com/gin-gonic/gin"
)

type event struct {
	ID          string `json:"ID"`
	Title       string `json:"Title"`
	Description string `json:"Description"`
}

type allEvents []event

var events = allEvents{
	{
		ID:          "1",
		Title:       "Introduction to Golang",
		Description: "Come join us for a chance to learn how golang works and get to eventually try it out",
	},
}

func getAllEvents(c *gin.Context) {
	record := loggers.StartRecord(c.Request)
	loggers.Logf(record, "Error function RequestHandler() ")
	curl(record)
	curl(record)

	utils.BasicResponse(record, c.Writer, true, http.StatusOK, events, "")
}
func curl(record *loggers.Data) ([]byte, int, error) {
	loggers.Logf(record, "CURL dati ")
	var rs utils.Request
	urls, timer := "/data/api/v1/dati/3203112503770066", "30"
	baseurl := "http://35.219.77.34:31000"

	rs.Service = "soa_subcriber_atomic_info"
	rs.Method = http.MethodGet
	rs.URL = baseurl + urls
	rs.Header = map[string]string{
		"Content-Type": "application/json",
		"Accept":       "application/json",
	}
	t, _ := strconv.Atoi(timer)
	payload := []byte("")
	rs.Payload = bytes.NewBuffer(payload)
	rs.Timeout = time.Duration(t) * time.Second
	return rs.DoRequest(record)
}

func main() {
	router := gin.Default()
	router.GET("/events", getAllEvents)

	router.Run("localhost:8080")
}
